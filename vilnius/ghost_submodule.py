import git
from git.objects import Submodule
from git.objects.submodule.util import sm_name

class GhostSubmodule(Submodule):
    #{ Iterable Interface

    @classmethod
    def safe_iter_items(cls, repo, parent_commit='HEAD'):
        """:return: iterator yielding Submodule instances available in the given repository"""
        pc = repo.commit(parent_commit)         # parent commit instance
        try:
            parser = cls._config_parser(repo, pc, read_only=True)
        except IOError:
            raise StopIteration
        # END handle empty iterator

        rt = pc.tree                                # root tree

        for sms in parser.sections():
            n = sm_name(sms)
            p = parser.get(sms, 'path')
            u = parser.get(sms, 'url')
            b = cls.k_head_default
            if parser.has_option(sms, cls.k_head_option):
                b = str(parser.get(sms, cls.k_head_option))
            # END handle optional information

            # get the binsha
            index = repo.index
            try:
                sm = rt[p]
            except KeyError:
                # try the index, maybe it was just added
                try:
                    entry = index.entries[index.entry_key(p, 0)]
                    sm = Submodule(repo, entry.binsha, entry.mode, entry.path)
                except KeyError:
                    yield (p, None)
                    continue
                # END handle keyerror
            # END handle critical error

            # fill in remaining info - saves time as it doesn't have to be parsed again
            sm._name = n
            if pc != repo.commit():
                sm._parent_commit = pc
            # end set only if not most recent !
            sm._branch_path = git.Head.to_full_path(b)
            sm._url = u

            yield (p, sm)
        # END for each section

    #} END iterable interface
