import os
from .ghost_submodule import GhostSubmodule
from .repository import VilniusRepository
from .participants import Participant
import yaml
import shutil
import appdirs
import git

APPNAME='vilnius'
APPAUTHOR='flaxandteal'

_commands = {
}

def command(name):
    def generator(f):
        _commands[name] = f
        return f
    return generator

class CommandHandler:
    _vilnius = None

    def __init__(self, printer):
        self._printer = printer
        self.appdir = appdirs.user_config_dir(APPNAME, APPAUTHOR)
        self.repo = os.path.join(self.appdir, 'projects')

        config_yaml = os.path.join(self.appdir, 'config.yaml')
        try:
            with open(config_yaml, 'r') as config_file:
                self.config = yaml.load(config_file)
            if self.config['account']:
                location = os.path.join(self.repo, 'participants', self.config['account'])
                self.participant = Participant(self.config['account'], location)
        except OSError:
            self.config = {
                'account': None
            }
            self.participant = None

    def run(self, command, *args, **kwargs):
        _commands[command](self, *args, **kwargs)

    @command("INITIALIZE")
    def do_initialize(self, *args):
        if len(args) != 2:
            self._printer("Need exactly two arguments (1) your participant code, and (2) pointing to the master repo")
            return

        repo = VilniusRepository.clone_from(args[1], self.repo, branch='master')
        os.makedirs(self.appdir, exist_ok=True)

        self.config['account'] = args[0]
        location = os.path.join(self.repo, 'participants', self.config['account'])
        self.participant = Participant(self.config['account'], location)
        with open(os.path.join(self.appdir, 'config.yaml'), 'w') as whoami:
            yaml.dump(self.config, whoami)

        modules = repo.update_submodules()

        for name, sm in modules.items():
            if sm:
                self._printer("Submodule: %s%s available" % (name, '' if sm.module_exists() else ' NOT'))
            else:
                self._printer("Submodule at %s not available" % name)

        if not self.participant.exists():
            repo.add_participant(participant, None)
            participant.setup()

        content = "[INITIALIZED]"
        self._printer(content)

    @command("CLEAN")
    def do_clean(self):
        shutil.rmtree(self.appdir)

        content = "[CLEANED]"
        self._printer(content)

    def get_vilnius(self):
        if not self._vilnius:
            self._vilnius = VilniusRepository.from_location(self.repo)

        return self._vilnius

    @command("PARTICIPANTS")
    def do_participants(self):
        repo = self.get_vilnius()
        participants = repo.get_participants()

        self._printer("Fetching participants...\n")

        content = ["PARTICIPANTS", "------------"]
        content += ["%s> %s" % (key, participant.name) for (key, participant) in participants.items()]

        self._printer("\n".join(content))

    @command("ADD_BIO")
    def do_add_bio(self, bio_name, markdown_file):
        repo = self.get_vilnius()
        self.participant.add_bio(markdown_file, bio_name)

    @command("PARTICIPANT")
    def do_participant(self, participant_name):
        repo = self.get_vilnius()
        participant = repo.get_participant(participant_name)

        self._printer("NAME: %s" % participant.name)
        self._printer("PUBLIC KEY: %s" % ('yes' if participant.get_public_key() else 'no'))
        bios = participant.get_bios()

        self._printer("\nBIOS")
        for bio in bios:
            self._printer(bio)

    @command("PARTICIPANT_ADD")
    def do_participant_add(self, participant, url):
        repo = self.get_vilnius()
        self._printer("Adding participant...\n")
        repo.add_participant(participant, url)

    @command("PULL")
    def do_pull(self):
        repo = self.get_vilnius()
        repo.do_pull()

    @command("PUSH")
    def do_push(self):
        repo = self.get_vilnius()
        repo.do_push()
