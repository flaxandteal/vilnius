import os
import git
from .ghost_submodule import GhostSubmodule
from .participants import Participant

class VilniusRepository:
    def __init__(self, repo):
        self._repo = repo

    def update_submodules(self):
        submodules = {}

        for p, sm in GhostSubmodule.safe_iter_items(self._repo):
            if sm:
                try:
                    sm.update(recursive=False)
                except:
                    pass

                submodules[sm.name] = sm
            else:
                submodules[p] = None

        return submodules

    @classmethod
    def clone_from(cls, url, location, branch='master'):
        repo = git.Repo.clone_from(url, location, branch=branch)
        this = cls.from_location(location)
        this.update_submodules()
        return this

    @classmethod
    def from_location(cls, location):
        repo = git.Repo(location)
        return cls(repo)

    def get_members(self):
        subrepos = {
            'participants': [],
            'proposals': []
        }

        for p, sm in GhostSubmodule.safe_iter_items(self._repo):
            if sm and sm.module_exists():
                subrepos[sm.path.split(os.path.sep)[0]].append(sm.module())

        return {
            'participants': {p.name: p for p in (Participant.from_repo(repo) for repo in subrepos['participants'])}
        }

    def get_participants(self):
        return self.get_members()['participants']

    def get_proposals(self):
        return self.get_members()['proposals']

    def get_participant(self, participant):
        return self.get_participants()[participant]

    def add_participant(self, participant, url):
        participants = self.get_members()['participants']
        repo = self._repo

        if participant in participants:
            raise RuntimeError("[ERROR: participant is already available]")

        GhostSubmodule.add(
            repo,
            participant,
            'participants/%s' % participant,
            url=url
        )

        repo.index.commit("Added new participant: %s" % participant)

    def do_pull(self):
        self._repo.remotes.origin.pull()
        self.update_submodules()

    def do_push(self):
        self._repo.remotes.origin.push()
