import os

class Participant:
    def __init__(self, name, repo):
        self.name = name
        self.location = repo.working_tree_dir
        self.repo = repo
        self.public_key = None

    def exists(self):
        return os.path.exists(self.location)

    def setup(self):
        os.makedirs(self.location)
        os.makedirs(os.path.join(self.location, 'bios'))

    @classmethod
    def from_repo(cls, repo):
        name = repo.git_dir.split(os.path.sep)[-1]
        participant = cls(name, repo)
        return participant

    def get_bios(self):
        bios = os.path.join(self.location, 'bios')
        return [bio for bio in os.listdir(bios) if bio.endswith('.md')]

    def add_bio(self, from_file, to):
        target = os.path.join(self.location, 'bios', '%s.md') % to
        shutil.copy(from_file, target)
        self.repo.index.add([target])
        self.repo.index.commit("add new bio: %s" bio_name)

    def get_public_key(self):
        public_key_file = os.path.join(self, self.location, 'public_key.asc')
        try:
            with open(public_key_file, 'r') as f:
                self.public_key = f.read()
        except FileNotFoundError:
            self.public_key = None

        return self.public_key
